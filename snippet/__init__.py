# -*- coding: utf-8 -*-
"""
A command-line interface for creating, inspecting, and editing
Bitbucket Snippets.
"""

from snippet import metadata


__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
